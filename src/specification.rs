use std::{
    collections::HashMap,
    fs::File,
    io::{BufReader, Error as IoError},
    path::Path,
};
use xml::{
    attribute::OwnedAttribute,
    reader::{Error as XmlError, EventReader, XmlEvent},
};

#[derive(Debug)]
pub(crate) struct Specification {
    // version: (u8, u8, u8),  // TODO
    tables: HashMap<String, Option<String>>,
}

#[derive(Debug)]
pub(crate) enum Error {
    TableTagHasNoName,
    EmptyStack,
    Io(IoError),
    Xml(XmlError),
}

impl From<XmlError> for Error {
    fn from(e: XmlError) -> Self {
        Error::Xml(e)
    }
}

impl From<IoError> for Error {
    fn from(e: IoError) -> Self {
        Error::Io(e)
    }
}

fn get_attrib(attributes: &Vec<OwnedAttribute>, key: &str) -> Option<String> {
    for attr in attributes {
        if attr.name.local_name == key {
            return Some(attr.value.to_owned());
        }
    }
    None
}

pub(crate) fn parse_specification_file<P: AsRef<Path>>(file: P) -> Result<Specification, Error> {
    let xml_file = BufReader::new(File::open(&file)?);
    let parser = EventReader::new(xml_file);
    let mut hierarchy: HashMap<String, Option<String>> = HashMap::new();
    let mut stack: Vec<String> = Vec::new();
    for event in parser {
        let event = event?;
        match event {
            XmlEvent::StartElement {
                ref name,
                ref attributes,
                ..
            } if name.local_name == "registro" => match get_attrib(attributes, "id") {
                Some(x) => stack.push(x.to_owned()),
                None => return Err(Error::TableTagHasNoName),
            },
            XmlEvent::EndElement { ref name, .. } if name.local_name == "registro" => {
                let child = match stack.pop() {
                    Some(x) => x,
                    None => return Err(Error::EmptyStack),
                };
                let parent = stack.last().map(|x| x.to_owned());
                hierarchy.insert(child, parent);
            }
            _ => {}
        }
    }
    Ok(Specification { tables: hierarchy })
}
